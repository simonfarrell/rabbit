Create a clustered RabbitMQ Stateful Set.
Deployment notes:

set the target namespace in the YAML files (defaults to "example")
in rabbitmq.conf 

edit the cluster-formation.k8s.hostname_suffix to replace
"example" with the target namespace


generate new cookie and username/password values for the StatefulSet

tune the memory resources defined in 3-statefulset.yml, which default to 800Mb
