FROM rabbitmq:3.8-alpine
RUN apk add curl
ADD rabbitmq.conf /etc/rabbitmq
ADD enabled_plugins /etc/rabbitmq

